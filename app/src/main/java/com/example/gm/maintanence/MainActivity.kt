package com.example.gm.maintanence

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity() {


    val activities = arrayOf<String>("Maintanance",
        "SlideActivity","WizardmenuActivity","WizardActivity","VehichleActivity","TrailorDimensionActivity",
        "TrailorDetectedActivity","TrailorAppAxlesActivity","StatusActivity","SetUpRemainderActivity",
        "PressureActivity","HighTechActivity","DetectedActivity","BrakeActivity")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1,activities)
        ListView.adapter = adapter

        ListView.onItemClickListener = object : OnItemClickListener {
            override fun onItemClick(adapter: AdapterView<*>, v: View, position: Int, arg3: Long)
            {
                when(position){
                    0-> startActivity(Intent(this@MainActivity,Maintanence::class.java))
                    1->startActivity(Intent(this@MainActivity, SlideActivity::class.java))
                    2->startActivity(Intent(this@MainActivity, WizardmenuActivity::class.java))
                    3->startActivity(Intent(this@MainActivity, WizardActivity::class.java))
                    4->startActivity(Intent(this@MainActivity, VehichleActivity::class.java))
                    5->startActivity(Intent(this@MainActivity, TrailorDimensionActivity::class.java))
                    6->startActivity(Intent(this@MainActivity, TrailorDetectedActivity::class.java))
                    7->startActivity(Intent(this@MainActivity, TrailorAppAxlesActivity::class.java))
                    8->startActivity(Intent(this@MainActivity, StatusActivity::class.java))
                    9->startActivity(Intent(this@MainActivity, SetUpRemainderActivity::class.java))
                   10->startActivity(Intent(this@MainActivity, PressureActivity::class.java))
                    11->startActivity(Intent(this@MainActivity, HighTechActivity::class.java))
                    12->startActivity(Intent(this@MainActivity, DetectedActivity::class.java))
                    13->startActivity(Intent(this@MainActivity, BrakeActivity::class.java))
                }





            }
        }

    }

}
